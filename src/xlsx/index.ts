import * as XLSX from "xlsx";

import config from "../config";

interface ReportRow {
  category: string;
  department?: string;
  process?: string;
  facility?: string;
  region?: string;
  country?: string;
  driveline?: string;
  year?: string;
  emissions: string;
  user: string;
  timestamp: number;
}

export async function parseExcel(
  { attachment, user, timestamp },
  ctx
): Promise<ReportRow[]> {
  ctx.logger.info(`Parse excel document to json`);

  // Read .xlsx file
  const { SheetNames, Sheets } = XLSX.read(attachment);

  // Get first sheet from WorkBook
  const sheet = Sheets[SheetNames[0]];
  const json = sheetToJson(sheet)
    .map(mapRow)
    // Append user and timestamp
    .map((row) => ({ ...row, user, timestamp }));

  if (!json.length) {
    throw new Error(`Error parsing excel document`);
  }

  return json;
}

// Convert xlsx to json - returns an array of rows
function sheetToJson(sheet): any[] {
  return XLSX.utils.sheet_to_json(sheet, config.xlsx.options);
}

function mapRow(row): ReportRow {
  // Define map for labels and how to map data
  const labelMap = {
    "GHG protocol category": "category",
    "Responsible department": "department",
    Process: "process",
    Facility: "facility",
    Region: "region",
    Country: "country",
    Driveline: "driveline",
    Year: "year",
    "Emissions [tCO2e]": "emissions", // This is the collected data ()
  };

  /* 
  If there is a need transform the data (maybe parse string to number) we could do something like this:
  
  Each label is a function where the data is passed. That way we can
  const labelDataMapper = {
    "GHG protocol category": (data) => ({ label: "category", data }),
    ...
    "Emissions [tCO2e]": (data) => ({ label: "emissions", data: Number(data) }),
  };
  
  And then in the reduce we get the function and run it with the data (row[key])
  const { label, data } = (labelDataMapper[key] || Function)(row[key]);
  */

  // Map each column in row
  return Object.keys(row).reduce((rows, col) => {
    const label = labelMap[col];
    if (!label) {
      throw new Error("No label for sheet row");
    }
    return {
      ...rows,
      [label]: row[col],
    };
  }, {} as ReportRow);
}
