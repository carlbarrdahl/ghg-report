import "reflect-metadata";
import assert from "assert";
import { parseExcel } from "./xlsx/index";
import { saveReport } from "./db";
import { saveBlob } from "./storage";
import {
  sendMail,
  createSenderSuccessMail,
  createSenderFailureMail,
  createOperationsSuccessMail,
} from "./mailer";

export async function parseAndStoreExcel({ sender, attachment }, ctx) {
  assert(typeof sender === "string", "Sender e-mail must be set");
  assert(attachment, "Excel document must be set");
  // Used as revision id
  const timestamp = Date.now();

  ctx.logger.info(`Parse and store excel document with revision: ${timestamp}`);

  // Parse report data
  return parseExcel({ attachment, user: sender, timestamp }, ctx)
    .then((rows) =>
      Promise.all([
        // Store excel sheet in blob storage
        saveBlob({ rows, attachment }, ctx),
        // Store report data in MSSQL
        saveReport(rows, ctx),
      ])
    )
    .then(() =>
      Promise.all([
        // Send email to data reporter
        sendMail(createSenderSuccessMail({ sender }), ctx),
        // Send email to operations
        sendMail(createOperationsSuccessMail({}), ctx),
      ])
    )
    .catch((err) => {
      ctx.logger.error(err);
      // Send error email to sender
      sendMail(createSenderFailureMail({ sender }), ctx);
    });
}
