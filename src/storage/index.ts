import {
  BlobServiceClient,
  StorageSharedKeyCredential,
} from "@azure/storage-blob";

import config from "../config";

export function createStorageClient() {
  const sharedKeyCredential = new StorageSharedKeyCredential(
    config.storage.account,
    config.storage.accountKey
  );
  return new BlobServiceClient(
    `https://${config.storage.account}.blob.core.windows.net`,
    sharedKeyCredential
  );
}

/* 
It might be better to move the logic for getting the blob container to createStorageClient.
However, that would make it async so `src/index.ts` would need to handle that.
*/
export async function saveBlob({ rows, attachment }, ctx) {
  ctx.logger.info(`Get container client: ${config.storage.containerName}`);
  const container = ctx.storage.getContainerClient(
    config.storage.containerName
  );
  // Create if it doesn't exist
  // if (!(await container.exist())) {
  //   const res = await container.create();
  //   ctx.logger.info(`Container created successfully: ${res.requestId}`);
  // }

  const fileName = fileNameFromReport(rows[0]);
  const client = container.getBlockBlobClient(fileName);

  ctx.logger.info(`Uploading blob: ${fileName}`);
  const res = await client.upload(attachment, Buffer.byteLength(attachment));

  ctx.logger.info(`Uploaded blob successfully: ${res.requestId}`);

  return { rows, attachment };
}

function fileNameFromReport({ category, timestamp }) {
  return `${category.toLowerCase().replace(/ /g, "_")}_${timestamp}.xlxs`;
}
