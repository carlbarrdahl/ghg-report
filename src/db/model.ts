import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Report {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  user!: string;

  @Column()
  timestamp!: number;

  @Column()
  category!: string;

  @Column()
  process!: string;

  @Column()
  facility!: string;

  @Column()
  region!: string;

  @Column()
  country!: string;

  @Column()
  driveline!: string;

  @Column()
  year!: string;

  @Column({ nullable: false })
  emissions!: number;
}
