import { createConnection, ConnectionOptions } from "typeorm";
import { Report } from "./model";

import config from "../config";

export async function createRepository() {
  const connection = await createConnection({
    ...config.typeorm.options,
    entities: [Report],
  } as ConnectionOptions);

  return {
    reports: connection.getRepository(Report),
  };
}

export async function saveReport(report, ctx) {
  ctx.logger.info(`Save report with category: ${report[0].category}`);
  return ctx.db.reports.save(report);
}
