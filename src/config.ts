const isDev = process.env.NODE_ENV !== "production";
require("dotenv").config();

export default {
  isDev,
  xlsx: {
    // Define what columns and rows to select in the report sheet.
    options: { range: "A9:I9999", defval: "" },
  },
  mailer: {
    operationsEmail: "operations@company.com",
    options: isDev
      ? { streamTransport: true, newline: "windows", logger: true }
      : {
          pool: true,
          host: "smtp.example.com",
          port: 465,
          secure: true, // use TLS
          auth: {
            user: "username",
            pass: "password",
          },
        },
  },
  storage: {
    account: process.env.STORAGE_ACCOUNT_NAME || "",
    accountKey: process.env.STORAGE_ACCOUNT_KEY || "",
    containerName: "ghg_reports",
  },
  typeorm: {
    options: {
      name: process.env.TYPEORM_NAME || "sqlite",
      type: process.env.TYPEORM_TYPE || "sqlite",
      host: process.env.TYPEORM_HOST || "host",
      port: process.env.TYPEORM_PORT || 1234,
      username: process.env.TYPEORM_USERNAME || "user",
      password: process.env.TYPEORM_PASSWORD || "pass",
      database: process.env.TYPEORM_DATABASE || "db",
      logging: isDev,
      synchronize: true,
    },
  },
};
