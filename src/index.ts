import { BlobServiceClient } from "@azure/storage-blob";
import { Transporter } from "nodemailer";
import { AzureFunction, Context, HttpRequest, Logger } from "@azure/functions";

import { createTransport } from "./mailer";
import config from "./config";
import { createStorageClient } from "./storage";
import { parseAndStoreExcel } from "./parseAndStore";

export interface IContext {
  mailer: Transporter;
  storage: BlobServiceClient;
  logger: Logger;
}

const httpTrigger: AzureFunction = async function (
  context: Context,
  req: HttpRequest
): Promise<void> {
  context.log(`HTTP trigger function processed a request`);
  try {
    const ctx: IContext = {
      mailer: createTransport(),
      storage: createStorageClient(),
      logger: context.log,
    };
    const { sender, attachment } = req.body;

    await parseAndStoreExcel({ sender, attachment }, ctx);

    context.res = { body: { success: true } };
  } catch (error) {
    context.log.error(error);
    context.res = {
      status: 500,
      body: config.isDev ? error.toString() : "Internal server error",
    };
  }
};

export default httpTrigger;
