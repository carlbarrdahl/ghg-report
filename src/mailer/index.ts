import nodemailer from "nodemailer";

import config from "../config";

export function createTransport() {
  return nodemailer.createTransport(config.mailer.options);
}

export async function sendMail(message, ctx) {
  ctx.logger.info(`Send e-mail with subject: ${message.subject}`);
  return ctx.mailer.sendMail(message);
}

// E-mail templates
export function createSenderSuccessMail({ sender }) {
  return {
    from: config.mailer.operationsEmail,
    to: sender,
    subject: "Report data successfully added",
    text: "",
    html: ``,
  };
}
export function createSenderFailureMail({ sender }) {
  return {
    from: config.mailer.operationsEmail,
    to: sender,
    subject: "Error handling excel report",
    text: "",
    html: ``,
  };
}
export function createOperationsSuccessMail({}) {
  return {
    from: config.mailer.operationsEmail,
    to: config.mailer.operationsEmail,
    subject: "Report data successfully added",
    text: "",
    html: ``,
  };
}
