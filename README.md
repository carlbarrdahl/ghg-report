# GHG Reporter

This is the Azure Function for the GHG reporter.

Here's the expected flow:

- Email is sent to data reporter (person filling out excel sheet)
- User replies to email with attached excel
- Azure logic app monitors incoming emails and trigger this function with sender email and attachment
- Parse excel sheet to json (if parsing fails, send email to sender to try again)
- Map json row data to database-friendly keys and values
- Save excel as blob in azure storage (`<report_category>_<timestamp>.xls`)
- Save report data in mysql
- Send success email to sender and operations

## TODO

- [ ] Finish TypeORM implementation in `src/db/index.ts`
- [ ] Add to context in `src/index.ts`

## Architecture

#### Folder structure

```sh
/src
    /db                # typeorm with mssql
    /mailer            # nodemailer for sending mails
    /storage           # azure blob storage
    /xlsx              # excel parser and data mapper
    index.ts           # azure function entry
    config.ts          # configuration
    parseAndStore.ts   # runs parseExcel, saveBlob, saveReport and sendEmail
```

#### Context

A context object is created with all the services and is passed to the functions. This makes it trivial to test as we can just create mocks in jest and assert they have been called with the right parameters, like this:

```ts
const ctx = {
  // ...,
  mailer: { sendMail: jest.fn() },
};
expect(ctx.mailer.sendMail).toHaveBeenCalledWith(
  expect.objectContaining({
    from: "operations@company.com",
    to: params.sender,
    subject: "Report data successfully added",
  })
);
```

## Tests

Put the excel report to test in `__tests__/test.xlsx`

```
npm run test
```
