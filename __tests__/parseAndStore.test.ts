import * as fs from "fs";

import { parseAndStoreExcel } from "../src/parseAndStore";

const storageUpload = jest.fn(() => ({ requestId: "test-requestId" }));
const getBlockBlobClient = jest.fn(() => ({ upload: storageUpload }));
const ctx = {
  storage: {
    getContainerClient: jest.fn(() => ({
      create: jest.fn(() => ({ requestId: "test-requestId" })),
      exist: jest.fn((bool) => bool),
      getBlockBlobClient,
    })),
  },
  db: { reports: { save: jest.fn() } },
  mailer: { sendMail: jest.fn() },
  logger: console,
};

describe("GHG Reporter", () => {
  beforeEach(() => {
    jest.restoreAllMocks();
  });
  test("parseAndStoreExcel valid document", (done) => {
    const attachment = fs.readFileSync(__dirname + "/test.xlsx");
    const params = {
      sender: "test@mail.com",
      attachment,
    };
    parseAndStoreExcel(params, ctx).then(() => {
      // Expect correct fileName
      expect(getBlockBlobClient).toHaveBeenCalledWith(
        expect.stringContaining("company_facilities_1604")
      );
      // Doc should be uploaded
      expect(storageUpload).toHaveBeenCalledWith(
        attachment,
        Buffer.byteLength(attachment)
      );

      // Data in doc should be inserted in db
      expect(ctx.db.reports.save).toHaveBeenCalledWith(expect.any(Array));

      // Verification email should be send to sender and operations
      expect(ctx.mailer.sendMail).toHaveBeenCalledWith(
        expect.objectContaining({
          from: "operations@company.com",
          to: params.sender,
          subject: "Report data successfully added",
        })
      );

      expect(ctx.mailer.sendMail).toHaveBeenCalledWith(
        expect.objectContaining({
          to: "operations@company.com",
          from: "operations@company.com",
          subject: "Report data successfully added",
        })
      );

      done();
    });
  });

  test("parseAndStoreExcel invalid document", (done) => {
    const params = {
      sender: "test@mail.com",
      attachment: "INVALID_DOCUMENT",
    };
    parseAndStoreExcel(params, ctx).then(() => {
      // Error email should be sent to sender
      expect(ctx.mailer.sendMail).toHaveBeenCalledWith(
        expect.objectContaining({
          to: params.sender,
          subject: "Error handling excel report",
        })
      );
      done();
    });
  });
});
