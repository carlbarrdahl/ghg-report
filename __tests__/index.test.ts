import * as fs from "fs";

import fn from "../src/index";

const context = {
  log: console,
};

test("Azure Function", async (done) => {
  expect(
    async () =>
      await fn(context as any, {
        body: {
          sender: "sender@mail.com",
          attachment: fs.readFileSync(__dirname + "/test.xlsx"),
        },
      })
  ).not.toThrow();

  done();
});
